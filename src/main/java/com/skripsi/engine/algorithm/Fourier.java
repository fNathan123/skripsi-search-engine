package com.skripsi.engine.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

import com.skripsi.engine.indexer.Posting;
import com.skripsi.engine.indexer.Trie;
import com.skripsi.engine.indexer.TrieNode;

public class Fourier {

    public DocScore[] rank;
    public Trie dictionary;
    public double[] docsLength;
    public double[] FF;
    public double spectrumPower;
    public String[] queryToken;

    public int length;

    public Fourier(int dictionarySize, Trie dictionary, double[] docsLength) {
        this.rank = new DocScore[dictionarySize];
        this.dictionary = dictionary;
        this.docsLength = docsLength;
        for (int i = 0; i < rank.length; i++) {
            rank[i] = new DocScore(i + 1, 0);
        }
    }

    public void resetRank() {
        for (int i = 0; i < rank.length; i++) {
            rank[i] = new DocScore(i + 1, 0);
        }
    }

    public double[] queryToSpectrum(String query) {
        this.length = 512;
        String[] token = query.toLowerCase().split(" ");
        // System.out.println("init: "+Arrays.toString(token));
        HashMap<String, Integer> qFreq = new HashMap<String, Integer>();
        LinkedList<String> tokenQueue = new LinkedList<String>();
        for (int i = 0; i < token.length; i++) {
            if (qFreq.containsKey(token[i])) {
                int oldValue = qFreq.get(token[i]);
                qFreq.put(token[i], oldValue++);
            } else {
                // System.out.println("put: "+token[i]);
                qFreq.put(token[i], 1);
                tokenQueue.add(token[i]);
            }
        }
        this.queryToken = new String[tokenQueue.size()];
        double[] qWeights = new double[qFreq.size()];
        int iterate = tokenQueue.size();
        for (int i = 0; i < iterate; i++) {
            String word = tokenQueue.poll();
            TrieNode temp = dictionary.search(word);
            // System.out.println("push: "+word);
            queryToken[i] = word;
            qWeights[i] = TF_IDF.getTFIDF(qFreq.get(word), temp.postingList.postingList.size(), docsLength.length);
        }
        // qWeights[0] = 0.585;
        // qWeights[1] = 0.585;
        // qWeights[2] = 1.585;
        while (length < queryToken.length * 300) {
            length *= 2;
        }

        double[] dt = new double[length * 2];

        for (int i = 0; i < dt.length; i++) {
            for (int j = 0; j < qWeights.length; j++) {
                int freq = (((300 * j) + 200) * 2) + 1;
                // System.out.println(freq+" "+length);
                dt[i] += qWeights[j] * Math.sin(freq * Math.PI * (i + 1) / (length * 2));
            }
        }
        // System.out.println(Arrays.toString(dt));
        return dt;

    }

    public void DFT(String query) {
        double[] dt = queryToSpectrum(query);
        // double dt[]={0f,1f,4f,9f};
        // double dt[] = {0,1,2,3,4,5,6,7,8};
        // this.length=2;

        double[] DFT = new double[length * 2];
        double[] FF1 = new double[length * 2];
        double[] FF2 = new double[length * 2];
        for (int i = 0; i < DFT.length; i++) {
            for (int j = 0; j < dt.length; j++) {
                FF1[i] += dt[j] * Math.cos(-1 * Math.PI * (i + 1) * (j + 1) / length);
                FF2[i] += dt[j] * Math.sin(-1 * Math.PI * (i + 1) * (j + 1) / length);
            }
            DFT[i] = Math.sqrt(Math.pow(FF1[i], 2) + Math.pow(FF2[i], 2));
            spectrumPower += DFT[i];
        }
        this.FF = DFT;
        // draw();

        // System.out.println("length:"+FF.length);
        // System.out.println(spectrumPower);
        // System.out.println(Arrays.toString(DFT));
    }

    public void filter() {
        int ZR, ZL;
        double breadth;
        ZL = 200;
        ZR = 201;
        System.out.println(FF.length);
        for (int i = 0; i < queryToken.length; i++) {
            ZL += i * 300;
            ZR += i * 300;
            TrieNode res = dictionary.search(queryToken[i]);
            ArrayList<Posting> postList = res.postingList.postingList;
            for (int j = 0; j < postList.size(); j++) {
                Posting temp = postList.get(j);
                int docIdx = temp.docID - 1;
                double termWeight = TF_IDF.getTFIDF(temp.termFreq, postList.size(), docsLength.length);
                breadth = 24 * (termWeight / docsLength[docIdx]);    
                // if (breadth % Math.floor(breadth) < 0.5) {
                //     breadth = Math.floor(breadth);
                // } else {
                //     breadth = Math.ceil(breadth);
                // }
                breadth = Math.round(breadth);
                // System.out.println("term:"+queryToken[i]+"in:"+docIdx+"breadth:"+breadth);
                int idxL = ZL - ((int) breadth);
                int idxR = ZR + ((int) breadth);
                double y1 = FF[idxL];
                double y2 = 0;
                double x1 = idxL;
                double x2 = ZL;
                for (int k = idxL; k <= ZL; k++) {
                    if(idxL!=ZL)rank[docIdx].score += FF[k]-((((k - x1) / (x2 - x1)) * (y2 - y1)) + y1);
                    else rank[docIdx].score+=FF[k];
                    // System.out.println("docleft:"+docIdx+" "+(FF[k]-((((k - x1) / (x2 - x1)) * (y2 - y1)) + y1)));
                    // System.out.println("k:"+k+" "+FF[k]);
                }
                y1 = 0;
                y2 = FF[idxR];
                x1 = ZR;
                x2 = idxR;
                for (int k = ZR; k <= idxR; k++) {
                    if(idxL!=ZL)rank[docIdx].score += FF[k]-(((k - x1) / (x2 - x1)) * (y2 - y1)) + y1;
                    else rank[docIdx].score+=FF[k];
                    // System.out.println("docright:"+docIdx+" "+(FF[k]-((((k - x1) / (x2 - x1)) * (y2 - y1)) + y1)));
                    // System.out.println("k:"+k+" "+FF[k]);
                }
                // if(i>0)System.out.println("peak:"+FF[ZL]+" "+FF[ZR]);
            }
            ZL = 200;
            ZR = 201;

        }
    }

    public void FFT(String query) {
        double[] dt=queryToSpectrum(query);
        // double dt[] = { 0f, 1f, 4f, 9f };
        // double dt[] = {0,1,2,3,4,5,6,7,8};
        // this.length = 2;
        Complex[] temp = new Complex[length*2];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = new Complex(dt[i], 0);
        }
        Complex[] res = FFT2(temp);
        double[] res2 = new double[length*2];
        for (int i = 0; i < res.length; i++) {
            res2[i] = Math.sqrt(Math.pow(res[i].real, 2) + Math.pow(res[i].imaginer, 2));
        }
        // this.FF = res2;
        this.FF=new double[length*2];
        for (int i = 0; i < res2.length; i++) {
            FF[i]=res2[res2.length-1-i];
        }
        // System.out.println(res2.length);
        // System.out.println(FF[47]);
        // System.out.println("FFT "+Arrays.toString(FF));
        // draw();

    }

    public Complex[] FFT2(Complex[] x) {
        int n = x.length;
        if (n == 1) {
            return new Complex[] { x[0] };
        }
        Complex[] odd = new Complex[n / 2];
        Complex[] even = new Complex[n / 2];
        for (int k = 0; k < n / 2; k++) {
            even[k] = x[2 * k];
            odd[k] = x[2 * k + 1];
        }
        Complex[] evenNew = FFT2(even);
        Complex[] oddNew = FFT2(odd);

        Complex[] y = new Complex[n];
        for (int k = 0; k < n / 2; k++) {
            double epsilon = -2 * k * Math.PI / n;
            Complex sn = new Complex(Math.cos(epsilon), Math.sin(epsilon));
            y[k] = evenNew[k].addition(sn.times(oddNew[k]));
            y[k + n / 2] = evenNew[k].subtraction(sn.times(oddNew[k]));
        }
        return y;
    }

    public void draw(){
        JFrame frame =new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new Graph(this.FF));
        frame.setSize(400,400);
        frame.setLocation(200,200);
        frame.setVisible(true);
    }

}

class Complex {
    double real;
    double imaginer;

    public Complex(double real, double imaginer) {
        this.real = real;
        this.imaginer = imaginer;
    }

    public Complex times(Complex b) {
        // double realTemp = 0;
        // double imaginaryTemp = 0;

        // realTemp += (num.real * this.real) - (this.imaginer * num.imaginer);
        // imaginaryTemp += (num.real * this.imaginer) + (num.imaginer * this.real);
        // return new Complex(realTemp, imaginaryTemp);
        Complex a = this;
        double real = a.real * b.real - a.imaginer * b.imaginer;
        double imag = a.real * b.imaginer + a.imaginer * b.real;
        return new Complex(real, imag);
    }

    public Complex addition(Complex b) {
        Complex a = this;            
        double real = a.real + b.real;
        double imag = a.imaginer + b.imaginer;
        return new Complex(real, imag);
        // return new Complex(this.real + num.real, num.imaginer + this.imaginer);
    }

    public Complex subtraction(Complex b) {
        // return new Complex(this.real - num.real, num.imaginer - this.imaginer);
        Complex a = this;
        double real = a.real - b.real;
        double imag = a.imaginer - b.imaginer;
        return new Complex(real, imag);
    }
}


class Graph extends JPanel{
    double[] coordinates={100,20,30,50};
    int mar=50;

    public Graph(double[] data){
        this.coordinates=data;
    }
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g1=(Graphics2D)g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        int width=getWidth();
        int height=getHeight();
        g1.draw(new Line2D.Double(mar,mar,mar,height-mar));
        g1.draw(new Line2D.Double(mar,height-mar,width-mar,height-mar));
        double x=(double)(width-2*mar)/(coordinates.length-1);
        double scale=(double)(height-2*mar)/getMax();
        g1.setPaint(Color.BLUE);
        for(int i=0;i<coordinates.length;i++){
            double x1=mar+i*x;
            double y1=height-mar-scale*coordinates[i];
            g1.fill(new Ellipse2D.Double(x1-2,y1-2,4,4));
        }
        
        
        
    }
    private double getMax(){
        double max=-Double.MAX_VALUE;
        int loc=0;
        for(int i=0;i<coordinates.length;i++){
            if(coordinates[i]>max){
                max=coordinates[i];
                loc=i;
            }
           
        }
        // System.out.println("max:"+max+" at"+loc+" length:"+coordinates.length);
        return max;
    }       
        
}