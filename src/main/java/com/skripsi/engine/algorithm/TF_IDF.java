package com.skripsi.engine.algorithm;


public class TF_IDF{

    public static double getTFIDF(int termFrequency,int postingLength,int totalDoc){
        double IDF=((Math.log(((double)totalDoc+1)/postingLength))/Math.log(2));
        // System.out.println("TF:"+termFrequency+" "+"IDF:"+IDF);
        return Math.log10(1+termFrequency)*IDF;
    }

    public static double getIDF(int postingLength,int totalDoc){
        return (Math.log((double)totalDoc+1)/Math.log(2))/postingLength;
    }

    public static double getTF(int termFrequency){
        return Math.log10(1+termFrequency);
    }
}