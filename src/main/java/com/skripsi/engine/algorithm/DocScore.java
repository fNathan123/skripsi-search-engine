package com.skripsi.engine.algorithm;

public class DocScore implements Comparable<DocScore>{
    public int DocID;
    public double score;

    public DocScore(int DocID,double score){
        this.DocID=DocID;
        this.score=score;
    }

    @Override
    public int compareTo(DocScore o) {
        if(o.score>this.score)return 1;
        else if(o.score<this.score)return -1;
        else return 0;
    }
}