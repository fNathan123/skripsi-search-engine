package com.skripsi.engine.algorithm;

import java.util.HashMap;
import java.util.Map;

import com.skripsi.engine.indexer.Posting;
import com.skripsi.engine.indexer.Trie;
import com.skripsi.engine.indexer.TrieNode;

public class VSM{
    public DocScore[] rank;
    private Trie dictionary;
    private double[] docsLength;

    public VSM(int dictionarySize,Trie dictionary,double[] docsLength){
        this.rank=new DocScore[dictionarySize];
        for (int i = 0; i < rank.length; i++) {
            rank[i]=new DocScore(i+1, 0);
        }
        this.dictionary=dictionary;
        this.docsLength=docsLength;
    }

    public void reset(){
        for (int i = 0; i < rank.length; i++) {
            rank[i]=new DocScore(i+1, 0);
        }
    }

    public void rankDocs(String query){
        String lower=query.toLowerCase();
        String[] tokens=lower.split(" ");
        HashMap<String,Integer> map=new HashMap<String,Integer>();
        HashMap<String,Double> tokenIDF=new HashMap<String,Double>();
        double queryLength=0;
        for (int x = 0; x < tokens.length; x++) {
            if(map.containsKey(tokens[x])){
                int oldValue=map.get(tokens[x]);
                map.put(tokens[x], oldValue++);
                // queryLength-=Math.pow(oldValue, 2);
                // queryLength+=Math.pow(oldValue++, 2);
            }else{
                map.put(tokens[x], 1);
                TrieNode res=dictionary.search(tokens[x]);
                tokenIDF.put(tokens[x], TF_IDF.getIDF(res.postingList.postingList.size(), this.rank.length));
                // queryLength++;
            }
        }
        for (Map.Entry<String,Integer> entry : map.entrySet()) {
            queryLength+=entry.getValue()*tokenIDF.get(entry.getKey());
        }
        queryLength=Math.sqrt(queryLength);

        for (int i = 0; i < tokens.length; i++) {
            TrieNode res=dictionary.search(tokens[i]);
            int postLength=res.postingList.postingList.size();
            int totalDoc=this.rank.length;
            //calculate query token weight
            // double tokenWeight=TF_IDF.getTFIDF(map.get(tokens[i]),postLength, totalDoc);
            double tokenWeight=map.get(tokens[i])*tokenIDF.get(tokens[i]);
            for(int j=0;j<res.postingList.postingList.size();j++){
                Posting temp=res.postingList.postingList.get(j);
                // System.out.println(temp.docID+" "+temp.termFreq);
                double weight=TF_IDF.getTFIDF(temp.termFreq, postLength, totalDoc);
                // if(temp.docID==7)System.out.println(tokens[i]+" "+temp.docID+" "+TF_IDF.getTF(temp.termFreq)+" "+TF_IDF.getIDF(postLength, totalDoc));
                // System.out.println("ID:"+temp.docID+" "+tokens[i]+" "+weight);
                rank[temp.docID-1].score+=(weight/docsLength[temp.docID-1])*(tokenWeight/queryLength);
                // if(temp.docID==7)System.out.println(rank[temp.docID-1].score);
            }

        }

    }
}