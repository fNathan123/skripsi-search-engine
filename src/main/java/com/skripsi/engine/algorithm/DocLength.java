package com.skripsi.engine.algorithm;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.skripsi.engine.indexer.Path;
import com.skripsi.engine.indexer.Posting;
import com.skripsi.engine.indexer.PostingList;
import com.skripsi.engine.indexer.Trie;
import com.skripsi.engine.indexer.TrieNode;

public class DocLength{

    public double[] docsLength;
    private Trie trie;
    public int docNum;

    public DocLength(int docNum,Trie trie){
        this.docsLength=new double[docNum];
        this.trie=trie;
        if(isIndexed()){
            this.readJSONLength();
        }else{
            writeJSON();
        }
        this.docNum=docNum;
    }

    public void addLength(int idx,double value){
        this.docsLength[idx]+=value;
    }

    public double getLength(int idx){
        return Math.sqrt(this.docsLength[idx]);
    }

    @SuppressWarnings("unchecked")
    public void writeJSON(){
        readLength(this.trie.getRoot());
        //write json
        JSONArray lengthList = new JSONArray();
        for (int i = 0; i < docsLength.length; i++) {
            JSONObject length = new JSONObject();
            double temp=getLength(i);
            length.put("DocID", i);
            length.put("length", temp);
            lengthList.add(length);
        }
        try (FileWriter file = new FileWriter(Path.getJsonPath()+"/DocLength.json")) {
            file.write(lengthList.toJSONString());
            file.flush();

        }catch(Exception e){
            System.out.println("error in doc length writer :"+e.getMessage());
        }
        
        
    }

    public boolean isIndexed(){
        try{
            File file=new File(Path.getJsonPath()+"/DocLength.json");
            return file.isFile();
        }catch(Exception e){
            System.out.println("file not indexed");
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public void readJSONLength(){

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(Path.getJsonPath()+"/DocLength.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
 
            JSONArray lengthList = (JSONArray) obj;
             
            //Iterate
            System.out.println(lengthList.size());
            lengthList.forEach( emp -> {
                int id=parseDocID((JSONObject) emp);
                this.docsLength[id]=parseLength((JSONObject)emp);
            });
 
        } catch (Exception e) {
           System.out.println("error from reader");
           e.printStackTrace();
        }
    }

    public int parseDocID(JSONObject obj){
        int docId=Integer.parseInt(obj.get("DocID")+"");
        return docId;
    }

    public double parseLength(JSONObject obj){
        double length=(double) obj.get("length");
        return length;
    }

    public void readLength(TrieNode parent){
        TrieNode[] child=parent.children;
        for (int i = 0; i < child.length; i++) {
            if(child[i]!=null)readLength(child[i]);
        }
        PostingList list=parent.postingList;
        ArrayList<Posting> postList=list.postingList;
        for (int i = 0; i < postList.size(); i++) {
            Posting temp=postList.get(i);
            double weight=TF_IDF.getTFIDF(temp.termFreq, postList.size(), docNum);
            docsLength[temp.docID-1]+=Math.pow(weight,2);
        }
    }
}