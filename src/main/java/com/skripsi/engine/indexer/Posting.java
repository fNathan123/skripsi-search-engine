package com.skripsi.engine.indexer;

public class Posting implements Comparable<Posting>{
    public int docID;
    public int termFreq;

    public Posting(int docID,int termFreq){
        this.docID=docID;
        this.termFreq=termFreq;
    }

    @Override
    public int compareTo(Posting o) {
        if(o.termFreq>this.termFreq)return 1;
        else if(o.termFreq<this.termFreq)return -1;
        else return 0;
    }
}