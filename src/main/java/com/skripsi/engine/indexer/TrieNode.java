package com.skripsi.engine.indexer;

public class TrieNode{
    public TrieNode[] children;
    // public int postIdx;
    public PostingList postingList;

    public TrieNode(){
        this.children = new TrieNode[26];
        // this.postIdx=postIdx;
        this.postingList=new PostingList();
    }
}