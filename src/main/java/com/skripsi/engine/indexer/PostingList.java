package com.skripsi.engine.indexer;

import java.util.ArrayList;
import java.util.Collections;

public class PostingList{
    public ArrayList<Posting> postingList;
    
    public PostingList(){
        postingList=new ArrayList<Posting>();
    }

    public int addPosting(int docID,int termFreq){
        postingList.add(new Posting(docID, termFreq));
        return postingList.size()-1;
    }

    public void updateSize(int docID,int value){
        // Posting temp=postingList.get(postID);
        // temp.termFreq+=value;
        int idx=0;
        boolean exist=false;
        Posting cur=null;
        if(postingList.size()!=0){
            cur=postingList.get(idx);
            while(true){
                if(cur!=null && cur.docID==docID){
                    cur.termFreq+=value;
                    exist=true;
                }
                idx++;
                if(idx>=postingList.size()){
                    break;
                }
                cur=postingList.get(idx);
            }
        }
        if(!exist)addPosting(docID, value);
        Collections.sort(postingList);
        
    }

}


