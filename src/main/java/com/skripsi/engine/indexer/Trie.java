package com.skripsi.engine.indexer;

public class Trie{
    private TrieNode root;

    public Trie(){
        this.root=new TrieNode();
    }

    public void insert(String word,int docID){
        String lcase=word.toLowerCase();
        TrieNode cur=root;
        // System.out.println("word: "+word);
        for (int i = 0; i < lcase.length(); i++) {
            char c=lcase.charAt(i);
            int index=c-'a';
            // System.out.println("char idx: "+c+" "+index);
            if(cur.children[index]==null){
                // if(word.equalsIgnoreCase("to"))System.out.println("inserted char: "+c);
                cur.children[index]=new TrieNode();
                cur=cur.children[index];
            }
            else{
                cur=cur.children[index];
            }
        }
        // if(cur.postIdx==-1){
        //     cur.postingList.addPosting(docID, 1);
        // }
        // else cur.postingList.updateSize(cur.postIdx, docID);
        if(!word.isEmpty())cur.postingList.updateSize(docID,1);
        // System.out.println("cur posidx: "+cur.postIdx);

    }

    public TrieNode search(String word){
        word=word.replaceAll("[^a-zA-Z]", "");
        TrieNode cur = root;
        for(int i=0; i<word.length(); i++){
            char c= word.charAt(i);
            int index = c-'a';
            if(cur.children[index]!=null){
                cur = cur.children[index];
            }
            else{
                // System.out.println("isi root:"+root.postingList.postingList.size());
                return root;
            }
        }
        return cur;
    }

    public TrieNode getRoot(){
        return this.root;
    }


}

