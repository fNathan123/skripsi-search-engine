package com.skripsi.engine.indexer;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Indexer{

    private final String docsNameInit="/Doc";
    public Trie dictionary;
    public int dictionarySize;

    public Indexer(){
        this.dictionary=new Trie();
    }

    public boolean checkForJobs(){
        JSONParser parser = new JSONParser();
        boolean status=false;
        try{
            String jsonPath=Path.getJsonPath();
            Object obj = parser.parse(new FileReader(jsonPath+"/notificator.json"));
            JSONObject n = (JSONObject) obj;
            status=(boolean) n.get("status");

        }catch(Exception e){
            System.out.println("error from Indexer :"+e.getMessage());
        }
        
        return status;
    }

    public void index(){
        String st;
        try{
            dictionarySize=0;
            int idx=1;
            while(true){
                String name=docsNameInit;
                int mark=idx/10;
                if(mark<1)name+="00";
                else if(mark>=1 && mark<10 )name+="0";
                name+=idx;
                // System.out.println(name);
                File file = new File(Path.getDocsPath()+name+".txt");
                if(!file.isFile())break;
                dictionarySize++;
                BufferedReader br = new BufferedReader(new FileReader(file));
                while ((st = br.readLine()) != null) {
                    // String replaced=st.replaceAll("[^a-zA-Z0-9]", " ");
                    String replaced=st.replaceAll("[^a-zA-Z]", " ");
                    // if(idx==31)System.out.println(replaced);
                    String[] temp=replaced.split(" ");
                    for (int i = 0; i < temp.length; i++) {
                        // if(temp[i].equalsIgnoreCase("from") && idx== 31)System.out.println(replaced +" "+ i);
                        dictionary.insert(temp[i], idx);
                    }
                    // br.close();
                } 
                br.close();
                idx++;
            }
            // System.out.println("hasil:"+dictionarySize);
            
        }catch(Exception e){
            System.out.println("error from indexer:"+e.getMessage());
        }
    }
}
