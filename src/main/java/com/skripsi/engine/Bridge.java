package com.skripsi.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.skripsi.engine.algorithm.Fourier;
import com.skripsi.engine.algorithm.DocScore;
import com.skripsi.engine.algorithm.VSM;
import com.skripsi.engine.indexer.Path;

public class Bridge{

    public static VSM vsm;
    public static Fourier fourier;

    public static String getDocument(int docID){
        String name="/Doc";
        String result="";
        int mark=docID/10;
        if(mark<1)name+="00";
        else if(mark>=1 && mark<10 )name+="0";
        name+=docID;
        // System.out.println(Path.getDocsPath()+name+".txt");
        File file = new File(Path.getDocsPath()+name+".txt");
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                result+=st;
            }
            br.close(); 
        }catch(Exception e){}

        return result;
    }

    public static String rank(int method,String query){
        String result="";
        DocScore[] rank;
        if(method==1){
            VSM vsm=Bridge.vsm;
            vsm.reset();
            vsm.rankDocs(query);
            rank=vsm.rank;
        }
        else{
            Fourier fourier=Bridge.fourier;
            fourier.resetRank();
            if(method==2)fourier.DFT(query);
            else fourier.FFT(query);
            fourier.filter();
            rank=fourier.rank;
        }
        // for (int i = 0; i < rank.length; i++) {
        //     System.out.println(rank[i].score);
        // }
        Arrays.sort(rank);
        int idx=0;
        String rec="";
        while(idx < rank.length && rank[idx].score>0){
            result+="<div class=\"col-lg-1\"></div> \n <div class=\"col-6\">";
            result+="<h4><a href=\"/doc/"+rank[idx].DocID+"\" >Doc "+rank[idx].DocID+"</a></h4>\n";
            result+="<span>Score:"+ rank[idx].score+"</span>";
            result+="<div class=\"mt2\">";
            // result+="<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n</div>\n</div>";
            String[] tokens=query.toLowerCase().split(" ");
            result+=getText(tokens, rank[idx].DocID);
            result+="<div class=\"col-lg-5\"></div>";
            System.out.println(rank[idx].DocID);
            rec+=rank[idx].DocID+",";
            idx++;
        }
        System.out.println(rec);
        return result;
    }

    public static String getText(String[] tokens,int docID){
        String replaced=getDocument(docID).replaceAll("[^a-zA-Z]", " ");
        String[] temp=replaced.split(" ");
        HashMap<String,Boolean> map=new HashMap<String,Boolean>();
        for (int i = 0; i < tokens.length; i++) {
            map.put(tokens[i], true);
        }
        List<Integer> pos=new ArrayList<Integer>();
        for (int i = 0; i < temp.length; i++) {
            if(map.containsKey(temp[i].toLowerCase())){
                temp[i]="<b>"+temp[i]+"</b>";
                map.remove(temp[i]);
                pos.add(i);
            }
        }
        String answer="<p>";
        int last=-100;
        for (int i = 0; i < pos.size(); i++) {
            int p=pos.get(i);
            if(p-1!=last){
                int left=p-3;
                if(left<0)left=0;
                int right=p+7;
                if(right>temp.length)right=temp.length;
                for(int j=left;j<right;j++){
                    answer+=temp[j]+" ";
                }
                if(right!=temp.length)answer+="...";
                answer+=" ";
            }
            last=p;
        }
        answer+="</p>\n</div>\n</div>";
        return answer;
    }
    
}