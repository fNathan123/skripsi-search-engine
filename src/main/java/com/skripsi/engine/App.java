package com.skripsi.engine;



import java.util.Arrays;

import com.blade.Blade;
import com.skripsi.engine.algorithm.Fourier;
import com.skripsi.engine.algorithm.DocLength;
import com.skripsi.engine.algorithm.DocScore;
import com.skripsi.engine.algorithm.VSM;
import com.skripsi.engine.indexer.*;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        
        Indexer indexer=new Indexer();
        indexer.index();
        // for(int i=0;i<t.postingList.postingList.size();i++){
        //     Posting temp=t.postingList.postingList.get(i);
        //     System.out.println(temp.docID+" "+temp.termFreq);
        // }
        DocLength docLength=new DocLength(indexer.dictionarySize, indexer.dictionary);
        //VSM
        VSM vsm=new VSM(indexer.dictionarySize, indexer.dictionary, docLength.docsLength);
        Bridge.vsm=vsm;
        // vsm.rankDocs("from fairest creatures");
        // DocScore[] rank=vsm.rank;
        // Arrays.sort(rank);
        // for (int i = 0; i < rank.length; i++) {
        //     System.out.println(rank[i].DocID+" "+rank[i].score);
        // }

        Fourier fourier=new Fourier(indexer.dictionarySize, indexer.dictionary, docLength.docsLength);
        Bridge.fourier=fourier;
        // dft.FFT2("from fairest creatures");
        // dft.DFT2("from fairest creatures");
        // dft.filter();
        // DocScore[] rank=dft.rank;
        // Arrays.sort(rank);
        // for (int i = 0; i < rank.length; i++) {
        //     System.out.println(rank[i].DocID+" "+rank[i].score);
        // }
        
        // for (int i = 0; i < docLength.docsLength.length; i++) {
        //     System.out.println(docLength.docsLength[i]);
        // }
        // fourier.DFT("asd");
        // fourier.FFT("asd");

        Blade.of().start(IndexController.class, args);
        
    }
}
