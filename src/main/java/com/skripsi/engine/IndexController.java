package com.skripsi.engine;


import com.blade.mvc.RouteContext;
import com.blade.mvc.annotation.GetRoute;
import com.blade.mvc.annotation.Param;
import com.blade.mvc.annotation.Path;
import com.blade.mvc.annotation.PathParam;


@Path
public class IndexController {

    @GetRoute("/") 
    public void printHtml(RouteContext ctx){
        // ctx.html("<center><h1>I Love Blade!</h1></center>");
        ctx.render("home.html");
    }

    @GetRoute("/search")
    public void searcher(@Param String query,@Param int method,RouteContext ctx){
        // System.out.println("person: " + query + " " +method);
        String result=Bridge.rank(method, query);
        ctx.attribute("result", result);
        ctx.attribute("query", query);
        ctx.render("searchRes.html");

    }

    @GetRoute("/doc/:docID")
    public void document(@PathParam Integer docID,RouteContext ctx){
        System.out.println(docID);
        String doc=Bridge.getDocument(docID);
        ctx.text(doc);
    }

    

} 